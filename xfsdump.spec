Name:      xfsdump
Version:   3.1.12
Release:   2
Summary:   Tools create and restore for the XFS filesystem
License:   GPLv2
URL:       http://xfs.org
Source0:   http://kernel.org/pub/linux/utils/fs/xfs/%{name}/%{name}-%{version}.tar.xz

BuildRequires: libtool, gettext, gawk
BuildRequires: xfsprogs-devel, ncurses-devel, libuuid-devel, libattr-devel
Requires:      xfsprogs, attr

Patch0:         0001-xfsdump-fix-memory-leak.patch

%description
xfsdump tools create and restore backups of directory trees on XFS filesystems.

%package        help
Summary:        Including man files for xfsdump
Requires:       man

%description    help
This contains man files for the using of xfsdump.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure

%make_build

%install
make DIST_ROOT=$RPM_BUILD_ROOT install

mv %{buildroot}/sbin/{xfsdump,xfsrestore} %{buildroot}/%{_sbindir}/
mkdir -p %{buildroot}/%{_sharedstatedir}/xfsdump/inventory

%find_lang %{name}

%files -f %{name}.lang
%license doc/COPYING
%doc README  doc/CHANGES doc/README.xfsdump
%exclude %{_datadir}/doc/xfsdump/
%{_sbindir}/*
%{_sharedstatedir}/xfsdump/inventory

%files help
%{_mandir}/man*/*


%changelog
* Wed Mar 20 2024 liuh <liuhuan01@kylinos.cn> - 3.1.12-2
- xfsdump: fix memory leak

* Wed Feb 8 2023 wuguanghao <wuguanghao3@huawei.com> - 3.1.12-1
- upgrade xfsdump version to 3.1.12-1

* Thu Oct 20 2022 yangchenguang <yangchenguang@uniontech.com> - 3.1.11-1
- update xfsdump version to 3.1.11-1

* Mon Jul 18 2022 zhanchengbin <zhanchengbin1@huawei.com> - 3.1.10-1
- update xfsdump version to 3.1.10-1

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 3.1.9-2
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Thu Jul 16 2020 wuguanghao <wuguanghao3@huawei.com> - 3.1.9-1
- update xfsdump version to 3.1.9-1

* Thu Oct 17 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.1.8-5
- Package init
